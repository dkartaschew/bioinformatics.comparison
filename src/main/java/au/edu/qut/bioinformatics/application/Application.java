/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.application;

import au.edu.qut.bioinformatics.dnasequence.DNASequence;
import au.edu.qut.bioinformatics.dnasequence.DNASequenceException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ElasticSearch Test application.
 *
 */
public class Application {

    /**
     * Application header.
     */
    private final static String ApplicationHeader =
            "DNA Sequence Comparison v1.0.0\n"
            + "------------------------------\n\n"
            + "Copyright 2013, Darran Kartaschew,\n"
            + "Apache License, Version 2.0\n\n"
            + "Package for testing ES and related activities.\n";
    /**
     * Application Help.
     */
    private final static String ApplicationHelp =
            "Usage: java -jar CompareSequence <file1> <file2> <k>\n"
            + "       <file1>  - The first file.\n"
            + "       <file2>  - The second file.\n"
            + "       <k>      - The length of kmers";
    /**
     * The first file to test.
     */
    private DNASequence sequence1;
    /**
     * The second file to test.
     */
    private DNASequence sequence2;
    /**
     * The third file to test.
     */
    private int kmerLength;

    /**
     * Main entry point.
     *
     * @param args CLI arguments.
     */
    public static void main(String[] args) {
        new Application().run(args);
    }

    /**
     * Main entry point.
     *
     * @param args CLI arguments.
     */
    public void run(String[] args) {
        printHeader();
        try {
            parseCLI(args);
        } catch (ApplicationException ex) {
            printHelp(ex.getMessage());
            System.exit(0);
        }
        printParameters();

        try {

            System.out.println("Sequence 1 : " + sequence1.getOrganismName());
            System.out.println("Sequence 2 : " + sequence2.getOrganismName());

            // Generate a map of kmers, noting the number of times a kmer exists.
            Map<String, Long> sequence1Map = getKmers(sequence1, kmerLength);
            Map<String, Long> sequence2Map = getKmers(sequence2, kmerLength);

            // Get the occurrances of each frequency
            Map<Long, Long> frequencies1 = getFrequencies(sequence1Map);
            Map<Long, Long> frequencies2 = getFrequencies(sequence2Map);
            saveMap(sequence1, "map1.csv", frequencies1);
            saveMap(sequence2, "map2.csv", frequencies2);
            frequencies1 = null;
            frequencies2 = null;
            System.gc();    // clean up the comparison map.

            // find the number of kmers not in each other.
            int unique1 = getUnique(sequence1Map, sequence2Map);
            int unique2 = getUnique(sequence2Map, sequence1Map);

            System.out.println("Item in Sequence 1 not in Sequence 2 : " + unique1);
            System.out.println("Item in Sequence 2 not in Sequence 1 : " + unique2);

            // Save the results to file.
            saveMap(sequence1, "kmermap1.csv", sequence1Map);
            saveMap(sequence2, "kmermap2.csv", sequence2Map);


        } catch (Exception ex) {
            /*
             * Use generation Exception here instead of specific Exceptions to ensure all transport
             * and internal exceptions for ES are caught here. (To list all exceptions would
             * be a very, very long list. (Any exception caught here is fatal, so exit).
             */
            System.out.println(ex.getMessage());
            System.exit(0);
        }
    }

    /**
     * Print application header information.
     */
    private void printHeader() {
        System.out.println(ApplicationHeader);
    }

    /**
     * Print Help to console for application usage.
     */
    private void printHelp(String message) {
        System.out.println("Warning: " + message);
        System.out.println(ApplicationHelp);
    }

    /**
     * Attempts to parse the command line for runtime parameters.
     *
     * @param args command line arguments
     * @throws Exception If the parsing fails in any manner.
     */
    private void parseCLI(String[] args) throws ApplicationException {
        if (args.length < 3) {
            throw new ApplicationException("Missing command line arguments");
        }
        try {
            sequence1 = new DNASequence(new File(args[0]));
            sequence2 = new DNASequence(new File(args[1]));
            kmerLength = Integer.parseInt(args[2]);
        } catch (NumberFormatException | DNASequenceException | FileNotFoundException ex) {
            throw new ApplicationException(ex);
        }
    }

    /**
     * Print all parameters as parsed to the command line.
     */
    private void printParameters() {
        System.out.println("Parameters:");
        System.out.println(" File 1 : " + sequence1.getFilename());
        System.out.println(" File 2 : " + sequence2.getFilename());
        System.out.println(" kmer   : " + kmerLength);
    }

    /**
     * Get a kmer from the test sequence.
     *
     * @param sequence The sequence to extract the kmer from.
     * @param start The start position of the kmer.
     * @param length The length of the kmer.
     * @return A string representing the kmer or "" if unable to represent.
     */
    public String kmer(DNASequence sequence, int start, int length) {
        if (start < 0 || length < 1 || start + length < sequence.getDNASequenceLength()) {
            try {
                return new String(sequence.getKMer(length, start));
            } catch (DNASequenceException ex) {
                Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "";
    }

    /**
     * Generate a HashMap of kmers and their frequency for the given kmer length
     *
     * @param seq The DNA sequence to extract the kmers from
     * @param length The kmer length to extract.
     * @return A Hashmap of kmers and their frequency
     */
    private Map<String, Long> getKmers(DNASequence seq, int length) {
        HashMap<String, Long> map = new HashMap<>();
        int seqSize = seq.getDNASequenceLength() - length;
        // extract the kmers and record their frequency
        for (int pos = 0; pos < seqSize; pos++) {
            String kmer = kmer(seq, pos, length);
            if (map.containsKey(kmer)) {
                Long value = map.get(kmer);
                value++;
                map.put(kmer, value);
            } else {
                map.put(kmer, new Long(1));
            }
        }
        return map;
    }

    /**
     * Generate a map of the occurrences of each frequency,
     *
     * @param map The map of terms and their frequency.
     * @return A map of the number of time each frequency occurs. (Key = frequency, value = number of times).
     */
    private Map<Long, Long> getFrequencies(Map<String, Long> map) {
        TreeMap<Long, Long> frequencies = new TreeMap<>();

        // Get a set of keys, and iterator over them
        Set<String> keys = map.keySet();
        for (String key : keys) {
            // For each key, get the frequency and add it to the frequency map.
            Long frequency = map.get(key);
            if (frequencies.containsKey(frequency)) {
                Long value = frequencies.get(frequency);
                value++;
                frequencies.put(frequency, value);
            } else {
                frequencies.put(frequency, new Long(1));
            }
        }
        return frequencies;
    }

    /**
     * Save the occurrences of the frequencies to a file.
     *
     * @param sequence The DNA Sequence
     * @param filename The filename to save the values to
     * @param map The map to extract the values from.
     */
    private void saveMap(DNASequence sequence, String filename, Map map) throws IOException {
        File file = new File(filename);
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file, true))) {
            out.write(sequence.toString() + "\n");
            out.write("Kmer Frequency, Occurrance Frequency\n");
            Set<Object> keys = map.keySet();
            for (Object key : keys) {
                out.write(String.format("%s,%d\n", key.toString(), map.get(key)));
            }
        }
    }

    /**
     * Subtract the items in map2 from map1, and the return the number of items left.
     *
     * @param map1 The first map
     * @param map2 The second map.
     * @return The number of items in map1, not in map2.
     */
    private int getUnique(Map<String, Long> map1, Map<String, Long> map2) {
        Set<String> keys = new HashSet<>(map1.keySet());
        Set<String> kmers = map2.keySet();
        for (String kmer : kmers) {
            keys.remove(kmer);
        }
        return keys.size();
    }
}
