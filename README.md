Comparison of DNA Sequences
============================================
----
Test bed application for comparison the frequency
of kmers in DNA sequences


Install
-------------

* run **$ mvn clean package** to build.
* java -jar target/CompareSequence-1.0.0-jar-with-dependencies.jar <file1> <file2> <k>

Running
-------------
The application will read both GenBank files, compare the frequencies of kmers and save
the frequencies into a file.
